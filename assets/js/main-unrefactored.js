var myHelpers = (function() {

    return {
        togglePanelVisibility: function ($panel, $bgLayer, $body) {
            if ($panel.hasClass('open')) {
                $panel.removeClass('open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
                    $body.removeClass('hide-overflow');
                });
                $bgLayer.removeClass('is-visible');
            }
            else {
                $panel.addClass('open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function() {
                    $body.addClass('hide-overflow');
                });
                $bgLayer.addClass('is-visible');
            }
        },

        toggleLightBox: function($activeClass, $trigger, $container, $close, $video) {

            $($trigger).on('click', function(event) {
                event.preventDefault();
                $($container).addClass($activeClass);
                $('body').addClass('hide-overflow');
                if ($video) {
                    $($video).get(0).play();
                }
            });

            $($container).on('click', function(event) {
                if ( $(event.target).is($close) || $(event.target).is($container) ) {
                    event.preventDefault();
                    $(this).removeClass($activeClass);
                    $('body').removeClass('hide-overflow');
                    if ($video) {
                        $($video).get(0).pause();
                    }
                }
            });

            $(document).keyup(function(event) {
                if (event.which == '27') {
                    $($container).removeClass($activeClass);
                    $('body').removeClass('hide-overflow');
                    if ($video) {
                        $($video).get(0).pause();
                    }
                }
            });
        },

        makeHeaderActive: function() {
            $('.mhc-header-element').addClass('active', 300);
            $('.mhc-header-logo-img').attr('src', 'assets/img/maplehill-grass-fed-logomark.png');
        },

        makeHeaderPassive: function() {
            $('.mhc-header-element').removeClass('active', 300);
            $('.mhc-header-logo-img').attr('src', 'assets/img/maplehill-grass-fed-logomark-white.png');
        },

        levelColumns: function($element) {

            $($element).each(function() {
                var $divs = $(this).add($(this).prev($element));

                var tallest = $divs.map(function(i, el) {
                    return $(el).height();
                }).get();
                $divs.height(Math.max.apply(this, tallest));
            });
        },

        handleTabs: function($tabLink, $activeClass, $tabContent) {

            var $parent;
            var tab;

            $tabLink.on('click', function(event) {
                $parent = $(this).parent();
                event.preventDefault();
                $parent.addClass($activeClass);
                $parent.siblings().removeClass($activeClass);
                tab = $(this).attr('href');
                $tabContent.not(tab).hide();
                $(tab).fadeIn('slow');
            });
        },

        handleTooltips: function($trigger, $activeClass, $container) {
            $trigger.on('click', function(event) {
                event.preventDefault();
                if ($(this).parent().hasClass($activeClass)) {
                    $trigger.parent().removeClass($activeClass);
                }
                else {
                    $trigger.parent().removeClass($activeClass);
                    $(this).parent().addClass($activeClass);
                }
            })
        },

        returnToTop: function($trigger, $scrollTop, $speed, $callback) {
            $trigger.on('click', function(event) {
                event.preventDefault();
                $('body, html').animate({
                    scrollTop: $scrollTop
                }, $speed, $callback);
            });
        },

        filterElements: function($trigger, $element, $scrollTop, $backToTopSpeed, $activeClass, $visibleClass, $div, $parentChildren, $hiddenClass, $parent, $children) {
            /*
                
                $trigger: the element that starts the event
                $element: the element that will be filtered
                $scrollTop: the distance from the top we want to page to scroll to when clicking the $trigger
                $backToToSpeed: how fast we want the page to scroll back up
                $activeClass: the class that the element should have when is active
                $visibleClass: the class that let us know if the element is or not visible, this is only to let us know. This doesn't actually make the product visible or not.
                $div: element we want to dissapear if it has no children <li>  
                $hiddenClass: class we want to apply to the hidden div ($div)
                $parent (optional): the default one is the actual element that is being filtered, 'parent' is the parent element of the filtered one
                $children: exists only if source is 'parent'
                
            */
            var currentClass, index, joinedFilters;
            var $element = $element;
            var filters = [];

            $trigger.on('click', function(event) {
                event.preventDefault();

                var $target = $(event.currentTarget);

                $('body, html').animate({ scrollTop: $scrollTop }, $backToTopSpeed);

                if ($target.hasClass($activeClass)) {
                    currentClass = $target.data('filter');

                    $element.filter(currentClass).fadeOut().removeClass($visibleClass);
                    if ($parent) {
                        $element.children($children).removeClass($visibleClass);
                    } 
                    $target.removeClass($activeClass)

                    index = filters.indexOf(currentClass);
                    filters.splice(index, 1);
                    joinedFilters = filters.join(', ');

                    if (filters.length === 0) {
                        $element.fadeIn().addClass($visibleClass);

                        if ($parent) {
                            $element.children($children).addClass($visibleClass);
                        }
                    }
                    else {
                        $element.filter(joinedFilters).fadeIn().addClass($visibleClass);

                        if ($parent) {
                            $element.children($children).addClass($visibleClass);
                        }
                    }
                }
                else {
                    $target.addClass($activeClass);
                    currentClass = $target.data('filter');
                    filters.push(currentClass);
                    joinedFilters = filters.join(', ');
                    $element.fadeOut().removeClass($visibleClass);
                    $element.filter(joinedFilters).fadeIn().addClass($visibleClass);

                    if ($parent) {
                        $element.children($children).addClass($visibleClass);
                    }
                }

                $($div).each(function(idx, item) {
                    var list    = $(item).children($parentChildren).children('li');
                    var visible = $(item).children($parentChildren).children('li.mhc-visible');
                    (visible.length == 0) ? $(item).closest($div).addClass($hiddenClass) : $(item).closest($div).removeClass($hiddenClass);
                });

            });
        }
    }
})();


/* GENERAL FUNCTIONS */
/* ================= */

var generalFunctions = (function() {
    'use strict';

    var mhc;

    var general = {

        settings: {
            body          : $('body'),
            header        : $('#mhcHeader'),
            headerEl      : $('.mhc-header-element'),
            logo          : $('.mhc-header-logo-img'),
            mainNavTrigger: $('.mhc-main-nav-trigger'),
            mainNavIcon   : $('.mhc-main-nav-icon'),
            mainNav       : $('.mhc-main-nav'),
            page          : $('.mhc-page'),

            hero          : $('#mhcHero'),
            heroBgHeight  : $('#mhcHeroBg').height(),
            headerHeight  : $('#mhcHeader').height(),

            backToTop     : $('#mhcBackToTop'),

            makeHeaderActive : myHelpers.makeHeaderActive.bind(myHelpers),
            makeHeaderPassive: myHelpers.makeHeaderPassive.bind(myHelpers),
            returnToTop      : myHelpers.returnToTop.bind(myHelpers),
        },

        init: function() {
            mhc = this.settings;
            this.videoLightboxHandler();
            this.headerStateHandler();
            this.navigationHandler();
            this.backToTopHandler();
        },

        videoLightboxHandler: function() {
            var modalHandler = myHelpers.toggleLightBox.bind(myHelpers);
            modalHandler('is-visible', '.mhc-modal-trigger-video', '.mhc-modal', '.mhc-modal-close', '#mhcVideo');
        },

        headerStateHandler: function() {
            $(document).ready(function() {
                if (mhc.hero.length === 0) {
                    mhc.makeHeaderActive();
                }
            });

            $(window).on('scroll', function() {
                var scroll = $(window).scrollTop();
                (scroll >= mhc.heroBgHeight - mhc.headerHeight) ? mhc.makeHeaderActive() : mhc.makeHeaderPassive();
            });
        },

        navigationHandler: function() {
            mhc.mainNavTrigger.on('click', function(event) {
                event.preventDefault();
                var scroll = $(window).scrollTop();

                if ( mhc.mainNav.hasClass('open') ) {
                    mhc.mainNav.removeClass('open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                        mhc.body.removeClass('overflow-hidden');
                    });
                    mhc.mainNavIcon.removeClass('clicked');
                    mhc.header.removeClass('open-nav');
                    mhc.page.removeClass('scaled-down');
                    if (scroll >= mhc.heroBgHeight) {
                        mhc.makeHeaderActive();
                    }
                } 
                else {
                    mhc.mainNav.addClass('open').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
                        mhc.body.addClass('overflow-hidden');
                    }); 
                    mhc.mainNavIcon.addClass('clicked');
                    mhc.makeHeaderPassive();
                    mhc.header.addClass('open-nav');
                    mhc.page.addClass('scaled-down');
                }
            });
        },

        backToTopHandler: function() {
            mhc.returnToTop(mhc.backToTop, 0, 800);
        }

    }
    general.init();
});


/* PRODUCTS FUNCTIONS */
/* ================== */

var productsFunctions = (function() {
    'use strict';

    var mhc;

    var products = {

        settings: {
            body                  : $('body'),
            accordionLink         : $('.mhc-products-accordion-link'),
            productsContainer     : $('#mhcProductsContainer'),
            category              : $('.mhc-products-category'),
            categoryOutput        : $('#mhcProductCategoryOutput'),
            productsCheckboxes    : $('#mhcProductsCheckboxes input'),
            product               : $('.mhc-product'),
            flavorBtn             : $('.mhc-flavor-btn'),
            categoryBtn           : $('.mhc-category-btn'),
            productFilters        : $('.mhc-products-option-selectors-container'),
            heroHeight            : $('#mhcHero').height(),
            headerHeight          : $('#mhcHeader').height(),
            filterElements        : myHelpers.filterElements.bind(myHelpers),
            scrollTop             : $('#mhcHero').height() - $('#mhcHeader').height()
        },

        init: function() {
            mhc = this.settings;
            this.accordionHandler();
            this.categoryHandler();
            this.flavorsHandler();
            this.lockProductFilters();
        },

        accordionHandler: function() {
            mhc.accordionLink.on('click', function(event) {
                event.preventDefault();

                var $parent = $(this).parent();
                var $panel = $parent.next();

                if ($parent.hasClass('opened')) {
                    $(this).find('.mhc-link-icon').text('+');
                    $panel.slideUp();
                    $parent.removeClass('opened');
                }
                else {
                    $(this).find('.mhc-link-icon').text('-');
                    $panel.slideDown();
                    $parent.addClass('opened');
                }
    
                return false;
            });
        },

        categoryHandler: function() {

            mhc.filterElements(mhc.categoryBtn, mhc.category, mhc.scrollTop, 800, 'checked', 'mhc-visible', '.mhc-products-category', 'ul.mhc-product-grid', 'hidden', true, 'li.mhc-product');

            // var $productsContainer = mhc.productsContainer.isotope({
            //     itemSelector: '.mhc-products-category'
            // });

            // mhc.productsCheckboxes.on('change', function() {
            //     var inclusives = [];
            //     var names      = [];

            //     var currentLists = mhc.productsCategory.find('li').length;

            //     if (currentLists === 0) {
            //         mhc.productsCategory.hide();
            //     }

            //     mhc.productsCheckboxes.each(function(i, elem) {
            //         if (elem.checked) {
            //             inclusives.push(elem.value);
            //             names.push(elem.name);
            //             console.log(inclusives);
            //             console.log(names);
            //         }
            //     });

            //     var filterValue = inclusives.length ? inclusives.join(', ') : '*';
            //     var filterName  = names.length      ?      names.join(', ') : '*';

            //     mhc.categoryOutput.text(filterName);
            //     mhc.productsContainer.isotope({
            //         filter: filterValue
            //     });

            //     // $('.mhc-products-category').each(function(idx, item) {
            //     //     // var list = $(item).find('ul.mhc-product-grid li:visible');
            //     //     var list = $(item).children('ul.mhc-product-grid').children('li');
            //     //     var visible = $(item).children('ul.mhc-product-grid').children('li.mhc-visible');
            //     //     console.log(visible);
            //     //     console.log("all: " + list.length);
            //     //     console.log("visible: " + visible.length);
            //     //     (visible.length == 0) ? $(item).closest('.mhc-products-category').addClass('hidden') : $(item).closest('.mhc-products-category').removeClass('hidden');
            //     // });
            // });

            // var currentClass, index, joinedFilters;
            // var $category = mhc.category;
            // var filters = [];

            // mhc.categoryBtn.on('click', function() {

            //     $('body, html').animate({ scrollTop: $('#mhcHero').height() - mhc.headerHeight }, 800);

            //     var $target = $(event.currentTarget);

            //     if ($target.hasClass('checked')) {
            //         // console.log('======================================================================');
            //         // console.log('removing class checked');

            //         currentClass = $target.data('filter');
            //         $category.filter(currentClass).fadeOut().removeClass('mhc-visible').children('li.mhc-product').removeClass('mhc-visible');
            //         $target.removeClass('checked');
                
            //         index = filters.indexOf(currentClass);
            //         filters.splice(index, 1);
            //         joinedFilters = filters.join(', ');
                    
            //         (filters.length == 0) ? $category.fadeIn('slow').addClass('mhc-visible').children('li.mhc-product').addClass('mhc-visible') : $category.filter(joinedFilters).fadeIn('slow').addClass('mhc-visible').children('li.mhc-product').addClass('mhc-visible');
            //     }
            //     else {
            //         // console.log('======================================================================');
            //         // console.log('adding class checked');
            //         $target.addClass('checked');
            //         currentClass = $target.data('filter');
            //         filters.push(currentClass);
            //         joinedFilters = filters.join(', ');
            //         $category.fadeOut('slow').removeClass('mhc-visible').children('li.mhc-product').removeClass('mhc-visible');
            //         $category.filter(joinedFilters).fadeIn('slow').addClass('mhc-visible').children('li.mhc-product').addClass('mhc-visible');
            //     }

            //     $('.mhc-products-category').each(function(idx, item) {
            //         // var list = $(item).find('ul.mhc-product-grid li:visible');
            //         var list    = $(item).children('ul.mhc-product-grid').children('li');
            //         var visible = $(item).children('ul.mhc-product-grid').children('li.mhc-visible');
            //         // console.log(visible);
            //         // console.log("all: " + list.length);
            //         // console.log("visible: " + visible.length);
            //         (visible.length == 0) ? $(item).closest('.mhc-products-category').addClass('hidden') : $(item).closest('.mhc-products-category').removeClass('hidden');
            //     });
            // });
        },

        flavorsHandler: function() {

            mhc.filterElements(mhc.flavorBtn, mhc.product, mhc.scrollTop, 800, 'checked', 'mhc-visible', '.mhc-products-category', 'ul.mhc-product-grid', 'hidden', false);

            // var currentClass, index, joinedFilters;
            // var $product = mhc.product;
            // var filters = [];

            // mhc.flavorBtn.on('click', function() {

            //     $('body, html').animate({ scrollTop: $('#mhcHero').height() - mhc.headerHeight }, 800);

            //     var $target = $(event.currentTarget);

            //     if ($target.hasClass('checked')) {
            //         // console.log('======================================================================');
            //         // console.log('removing class checked');

            //         currentClass = $target.data('filter');
            //         $product.filter(currentClass).fadeOut().removeClass('mhc-visible');
            //         $target.removeClass('checked');
                
            //         index = filters.indexOf(currentClass);
            //         filters.splice(index, 1);
            //         joinedFilters = filters.join(', ');
                    
            //         (filters.length == 0) ? $product.fadeIn('slow').addClass('mhc-visible') : $product.filter(joinedFilters).fadeIn('slow').addClass('mhc-visible');
            //     }
            //     else {
            //         // console.log('======================================================================');
            //         // console.log('adding class checked');
            //         $target.addClass('checked');
            //         currentClass = $target.data('filter');
            //         filters.push(currentClass);
            //         joinedFilters = filters.join(', ');
            //         $product.fadeOut('slow').removeClass('mhc-visible');
            //         $product.filter(joinedFilters).fadeIn('slow').addClass('mhc-visible');
            //     }

            //     $('.mhc-products-category').each(function(idx, item) {
            //         // var list = $(item).find('ul.mhc-product-grid li:visible');
            //         var list    = $(item).children('ul.mhc-product-grid').children('li');
            //         var visible = $(item).children('ul.mhc-product-grid').children('li.mhc-visible');
            //         // console.log(visible);
            //         // console.log("all: " + list.length);
            //         // console.log("visible: " + visible.length);
            //         (visible.length == 0) ? $(item).closest('.mhc-products-category').addClass('hidden') : $(item).closest('.mhc-products-category').removeClass('hidden');
            //     });
            // });
        },

        lockProductFilters: function() {

            if ($('.mhc-products-wrapper').length) {
                var fixProductFilters = function() {
                    mhc.productFilters.addClass('fixed');
                };

                var unfixProductFilters = function() {
                    mhc.productFilters.removeClass('fixed');
                };

                $(window).on('scroll', function() {
                    var scroll = $(window).scrollTop();
                    (scroll >= $('.mhc-products-wrapper').offset().top - mhc.headerHeight) ? fixProductFilters() : unfixProductFilters();
                });
            }            
        }

    }
    products.init();
});


/* ABOUT FUNCTIONS */
/* =============== */

var aboutFunctions = (function() {
    'use strict';

    var mhc;

    var about = {

        settings: {
            story : $('#mhcStory'),
            mission: $('#mhcMission'),
            timeline: $('#mhcTimeline'),
            storyLink: $('.mhc-callout-story'),
            missionLink: $('.mhc-callout-mission'),
            timelineLink: $('.mhc-callout-timeline')
        },

        init: function() {
            mhc = this.settings;
            this.activePageHandler();
        },

        activePageHandler: function() {
            $(document).ready(function() {
                if (mhc.story.length >= 1) {
                    mhc.storyLink.addClass('active');
                }
                else if (mhc.timeline.length >= 1) {
                    mhc.timelineLink.addClass('active');
                }
                else if (mhc.mission.length >= 1) {
                    mhc.missionLink.addClass('active');
                }
            });
        },
    }
    about.init();
});


/* GRASS-FED FUNCTIONS */
/* =================== */

var grassFedFunctions = (function() {
    'use strict';

    var mhc;

    var grassFed = {

        settings: {
            choreographyTabLink    : $('.mhc-choreography-tabs-menu-link'),
            choreographyTabContent : $('.mhc-choreography-tab-content'),
            paddocksTabLink        : $('.mhc-paddocks-tabs-menu-link'),
            paddocksTabContent     : $('.mhc-paddocks-tab-content'),
            tooltipTrigger         : $('.tooltip-trigger'),
            tooltipInfo            : $('.tooltip-info'),
            paddocksTabContent     : $('.mhc-paddocks-tab-content'),
            activeClass            : 'active',
            handleTabs             : myHelpers.handleTabs.bind(myHelpers),
            handleTooltips         : myHelpers.handleTooltips.bind(myHelpers)
        },

        init: function() {
            mhc = this.settings;
            this.choreographyTabsHandler();
            this.paddocksTabsHandler();
            this.chewOnThisTooltipsHandler();
            this.textOnShadowHandler();
        },

        choreographyTabsHandler: function() {
            $(document).ready(function() {
                mhc.handleTabs(mhc.choreographyTabLink, mhc.activeClass, mhc.choreographyTabContent);
            });
        },

        paddocksTabsHandler: function() {
            $(document).ready(function() {
                mhc.handleTabs(mhc.paddocksTabLink, mhc.activeClass, mhc.paddocksTabContent);
            });
        },

        chewOnThisTooltipsHandler: function() {
            $(document).ready(function() {
                mhc.handleTooltips(mhc.tooltipTrigger, mhc.activeClass, mhc.toolTipInfo);
            });
        },

        textOnShadowHandler: function() {
            $('.mhc-show-text-on-hover').on('click', function(event) {
                event.preventDefault();
                $(this).toggleClass('active');
            });
        }
    }
    grassFed.init();
});


/* FUNCTION TRIGGERS */
/* ================= */

(function() {
    generalFunctions();
    productsFunctions();
    aboutFunctions();
    grassFedFunctions();
})();